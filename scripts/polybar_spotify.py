#!/usr/bin/env python3
from dbus import SessionBus, Interface

spaces = 60
trunclen = 25
mplayer = 'org.mpris.MediaPlayer2.{}'

try:
    session_bus = SessionBus()
    spotify_bus = session_bus.get_object(
        mplayer.format('spotify'),
        '/org/mpris/MediaPlayer2'
    )

    spotify_properties = Interface(
        spotify_bus,
        'org.freedesktop.DBus.Properties'
    )

    metadata = spotify_properties.Get(
        mplayer.format('Player'),
        'Metadata'
    )

    artist = metadata['xesam:artist'][0]
    song = metadata['xesam:title']

    if len(song) > trunclen:
        song = song[0:trunclen]
        song += '...'
        if ('(' in song) and (')' not in song):
            song += ')'

    print(f'{" "*spaces}{artist}: {song}')

except Exception as e:
    print(e)
