from os.path import abspath
from shutil import copytree
import click


@click.group('configuration')
def config():
    ...


@config.group('backup')
def backup():
    ...


@config.group('install')
def install():
    ...


@install.command()
@clic.
def configure_i3():
    copytree(abspath('./i3'), '/home/dunossauro/.config/i3')


config()
